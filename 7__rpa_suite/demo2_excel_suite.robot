*** Settings ***
Library     RPA.Excel.Files
Library    Collections

*** Test Cases ***
TC1
    Open Workbook   path=C:${/}Automation Concepts${/}orange_data.xlsx
    @{sheets}    List Worksheets
     FOR    ${sheet}    IN    @{sheets}
         Log    ${sheet}

     END
TC2
    Open Workbook   path=C:${/}Automation Concepts${/}orange_data.xlsx
    ${sheet}    Read Worksheet     ASHISH
    Log To Console    ${sheet}
    Log To Console    ${sheet}[2][B]
TC3
    Open Workbook   path=C:${/}Automation Concepts${/}orange_data.xlsx
    Set Worksheet Value    4    1    test
    Set Worksheet Value    4    2    test123
    Set Worksheet Value    4    3    Invalid credentials
    Save Workbook
    ${sheet}    Read Worksheet     ASHISH
    Close Workbook
    Log   ${sheet}
    Log   ${sheet}[3][A]
    Log   ${sheet}[3][B]
    Log   ${sheet}[3][C]
    