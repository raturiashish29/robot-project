*** Settings ***
Library     RPA.Word.Application
Library    Collections

*** Test Cases ***
TC1
    Open Application
    Create New Document
    Write Text    text="hello ths is training on RPA"
    Save Document As    filname=C:${/}Automation Concepts${/}data.docx
    Quit Application
    
TC2
    Open Application
    Open File    filname=C:${/}Automation Concepts${/}data.docx
    ${text}     Get All Texts     
    Log To Console    ${text}

    