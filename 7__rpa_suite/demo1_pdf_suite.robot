*** Settings ***
Library     RPA.PDF
Library    Collections

*** Test Cases ***
TC1
    ${response}  Get Text From Pdf   source_path=C:${/}Automation Concepts${/}demofile.pdf
    Log  ${response}
    ${page}    Convert To Integer    1
    Log    ${response}[${page}]

    ${keys}     Get Dictionary Keys     ${response}
    FOR    ${key}    IN    @{keys}
        Log    ${response}[${key}]

    END

TC2
     Open Pdf    source_path=C:${/}Automation Concepts${/}demofile.pdf
    ${pages}    Get Number Of Pages
    Log    ${pages}
