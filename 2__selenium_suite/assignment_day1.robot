*** Settings ***
Library     SeleniumLibrary

*** Test Cases ***
TC1
    Open Browser    url=https://github.com/login    browser=chrome
    Input Text    id=login_field   hello
    Input Text    id=password   89hello
    Click Element    name=commit
    Element Should Contain    xpath=//div[contains(text(),'username or password')]   Incorrect username or password

TC2
    Open Browser    url=https://www.salesforce.com/in/form/signup/freetrial-sales/   browser=chrome
    Input Text    name=UserFirstName   john
    Input Text    name=UserLastName   wick
    Input Text    name=UserEmail  john@gmail.com
    Select From List By Label    name=UserTitle    IT Manager
    Input Text    name=CompanyName      Ericsson
    Select From List By Value    name=CompanyEmployees    350
    Select From List By Label    name=CompanyCountry   United Kingdom
    Scroll Element Into View    xpath=//input[@id='SubscriptionAgreement']
    ###Select Checkbox    xpath=//input[@id='SubscriptionAgreement']
    Click Element   xpath=//div[@class='checkbox-ui']
    Click Element    xpath=//button[@name='start my free trial']
    Element Text Should Be    xpath=//span[contains(@id,'UserPhone')]    Enter a valid phone number

TC3
    Open Browser    url=https://www.medibuddy.in    browser=chrome
    Set Selenium Implicit Wait    5
    Click Element    link=Login
    Click Element    xpath=//div[@class='coperate']
    Sleep    1
    Click Element    xpath=//div[@class='coperate']
    Input Text    name=userName    john

    Click Element    xpath=//div[@class='submitbtn']
    Input Text    name=password    john123
    Click Element    xpath=//span[text()='Show password']
    Click Element    xpath=//button[text()='Log in']


    