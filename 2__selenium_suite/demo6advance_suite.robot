
*** Settings ***
Library     SeleniumLibrary

*** Test Cases ***
TC1
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://www.ilovepdf.com/pdf_to_word
    Choose File    xpath=//input[@type='file']    C:${/}Automation Concepts${/}demofile.pdf

TC2Nasscom

    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://nasscom.in/about-us/contact-us
    Mouse Over    link=Membership
    Mouse Over    link=Become a Member
    Click Element    link=Membership Benefits

TC3 Php Javascript
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://phptravels.net/
    #checkin 19-08-2023
    Execute Javascript  document.querySelector('#checkin').value='19/08/2023'
    #checkout 26-08-2023
    Execute Javascript  document.querySelector('#checkout').value='26/08/2023'

    Execute Javascript  document.querySelector('#adults').value='2'
    Execute Javascript  document.querySelector('#childs').value='2'
    sleep   10s