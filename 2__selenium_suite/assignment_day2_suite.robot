*** Settings ***
Library     SeleniumLibrary

*** Keywords ***
Launch Browser
    [Arguments]     ${browser_name}     ${url}
    Open Browser    browser=${browser_name}
    Maximize Browser Window
    Set Selenium Implicit Wait    7s
    Go To    ${url}

*** Test Cases ***
TC1

    Launch Browser      chrome      http://demo.openemr.io/b/openemr/
    Input Text    css=#authUser    admin
    Input Text    css=#clearPass   pass
    Select From List By Value    css=select[name='languageChoice']   18
    Click Element    css=#login-button

    Click Element    xpath=//div[text()='Patient']
    Click Element    xpath=//div[text()='New/Search']

    Select Frame    xpath=//iframe[@name='pat']

    Input Text    name=form_fname    ashish
    Input Text    id=form_lname    raturi
    Input Text    id=form_DOB    2023-05-23
    Select From List By Label    id=form_sex    Male
    Click Element    id=create
    Unselect Frame
    Select Frame    xpath=//iframe[@id='modalframe']
    Click Element    xpath=//button[text()='Confirm Create New Patient']
    Unselect Frame
     ${alert_text}   Handle Alert    action=ACCEPT  timeout=20s
    Log To Console    ${alert_text}

    Run Keyword And Ignore Error  Click Element    css=.closeDlgIframe
    Select Frame    xpath=//iframe[@name='pat']
    Element Should Contain    xpath=//span[contains(text(),'Record Dashboard')]    ashish raturi
    Should Contain   ${alert_text}     Tobacco
    Unselect Frame


TC2
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    7s
    Go To    url=https://www.royalcaribbean.com/account/signin
#    Click Element    link=Sign In
#    Click Element    xpath=//a[normalize-space()='Create an account']
    Execute Javascript  document.querySelector('.login__create-account  login__create-account--royal).click()