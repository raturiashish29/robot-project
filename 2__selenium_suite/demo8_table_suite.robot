*** Settings ***
Library     SeleniumLibrary


*** Test Cases ***
TC1
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://datatables.net/extensions/select/examples/initialisation/checkbox.html
    ${name1}    Get Text    xpath=//table[@id='example']/tbody/tr[1]/td[2]
    Log To Console    ${name1}


TC2 ALL Name
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://datatables.net/extensions/select/examples/initialisation/checkbox.html
    FOR    ${i}    IN RANGE    1    11
        ${name}    Get Text    xpath=//table[@id='example']/tbody/tr[${i}]/td[2]
        Log To Console    ${name}
    END

TC3 If Condition
    #click on checkbox of Brenden Wagner
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://datatables.net/extensions/select/examples/initialisation/checkbox.html
    FOR    ${i}    IN RANGE    1    11
        ${name}    Get Text    xpath=//table[@id='example']/tbody/tr[${i}]/td[2]
        Log To Console    ${name}
        IF    '${name}'=='Brenden Wagner'
             Click Element    xpath=//table[@id='example']/tbody/tr[${i}]/td[1]
             Log    Found ${name}
             Exit For Loop
        END
    END

TC4 Run Keywords Example
    #click on checkbox of Brenden Wagner
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://datatables.net/extensions/select/examples/initialisation/checkbox.html
    FOR    ${i}    IN RANGE    1    11
        ${name}    Get Text    xpath=//table[@id='example']/tbody/tr[${i}]/td[2]
        Log To Console    ${name}

        Run Keyword If    '${name}'=='Brenden Wagner'   Click Element    xpath=//table[@id='example']/tbody/tr[${i}]/td[1]
        Exit For Loop If    '${name}'=='Brenden Wagner'

         Run Keyword If    '${name}'=='Brenden Wagner'
         ...  Run Keywords    Click Element    xpath=//table[@id='example']/tbody/tr[${i}]/td[1]  AND   Log    Found ${name}
    END

TC5 Contains Check
    #click on checkbox of Brenden Wagner
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://datatables.net/extensions/select/examples/initialisation/checkbox.html
    FOR    ${i}    IN RANGE    1    11
        ${name}    Get Text    xpath=//table[@id='example']/tbody/tr[${i}]/td[2]
        Log To Console    ${name}
        IF   'Bradley' in '${name}'
             Click Element    xpath=//table[@id='example']/tbody/tr[${i}]/td[1]
             Log    Found ${name}
             Exit For Loop
        END
    END


TC6 Contains Check
    #click on checkbox of Brenden Wagner
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://datatables.net/extensions/select/examples/initialisation/checkbox.html
    #run below code for all pages
    #print names from  all pages

    ${row_count}    Get Element Count    xpath=//table[@id='example']/tbody/tr
    FOR    ${i}    IN RANGE    1    ${row_count}+1
        ${name}    Get Text    xpath=//table[@id='example']/tbody/tr[${i}]/td[2]
        Log To Console    ${name}
        IF   'Thor' in '${name}'
             Click Element    xpath=//table[@id='example']/tbody/tr[${i}]/td[1]
             Log    Found ${name}
             Exit For Loop
        END
    END

TC7 Contains Check
    #click on checkbox of Brenden Wagner
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://datatables.net/extensions/select/examples/initialisation/checkbox.html
    ${name}    Get Text    xpath=//table[@id='example']/tbody
    Log To Console    ${name}

TC8 Using Get WebElements - Will explain later
    #click on checkbox of Brenden Wagner
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://datatables.net/extensions/select/examples/initialisation/checkbox.html
    @{elements}    Get WebElements    xpath=//table[@id='example']/tbody/tr
    Log To Console    ${elements}
    Log     ${elements}
    Log    ${elements}[0]
    ${row1_text}    Get Text    ${elements}[0]
    Log  ${row1_text}

    ${count}    Get Length    ${elements}

    #iterate every element (row) and getting the text
    FOR    ${i}    IN RANGE    0    ${count}
        ${row_text}    Get Text    ${elements}[${i}]
        Log  ${row_text}

    END

