*** Settings ***
Library     SeleniumLibrary

*** Test Cases ***
TC1
    Open Browser    browser=chrome
    Maximize Browser Window
    Go To    url=https://db4free.net

    Click Element    partial link=phpMyAdmin
    Switch Window    phpMyAdmin
    Input Text    id=input_username    admin
    Input Text    id=input_password    admin123
    Click Element    id=input_go
    Element Should Contain    id=pma_errors  Access denied for user

TC2
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20
    Go To    url=https://www.online.citibank.co.in/
    Click Element    xpath=//div/div/a[@class='newclose']
    Click Element    xpath=//div/div/a[@class='newclose2']
    Click Element    id=loginId
    Switch Window    Citibank India
    Click Element    xpath=//div[contains(text(),'Forgot User ID')]
    Click Element    link=select your product type
    Click Element    link=Credit Card
#    Input Text    xpath=//input[@id='citiCard1']    5456
#    Input Text    xpath=//input[@id='citiCard2']    5688
#    Input Text    xpath=//input[@id='citiCard3']    8799
#    Input Text    xpath=//input[@id='citiCard4']    7998
    Input Text    css=#citiCard1    5456
    Input Text    css=#citiCard2    5688
    Input Text    css=#citiCard3    8799
    Input Text    css=#citiCard4   7998
#    Input Text    id=cvvnumber   123
    Input Text    css=input[name='CCVNO']   123
    Click Element    id=bill-date-long
    Select From List By Value    class=ui-datepicker-year  2022
    Select From List By Label    class=ui-datepicker-month  Apr

    Click Element    link=7
    Execute Javascript  document.querySelector('#bill-date-long').value='14/04/1998'
    Click Element    xpath=//input[@value='PROCEED']
    Element Should Contain    xpath=//li[contains(text(),'Terms and Conditions')]    Please accept Terms and Conditions

    
    
    