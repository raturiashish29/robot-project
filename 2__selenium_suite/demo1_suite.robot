*** Settings ***
Library     SeleniumLibrary

*** Test Cases ***
TC1
    Open Browser    url=https://www.facebook.com     browser=chrome
    ${current_title}    Get Title
    Log To Console    ${current_title}
    Input Text    id=email    "raturiashish@gmail.com"
    Input Text    id=pass    "January@29"
    Click Element    name=login
    Close Browser

TC2
    Open Browser    url=https://www.facebook.com     browser=chrome
    ${current_title}    Get Title
    Log To Console    ${current_title}
    Set Selenium Implicit Wait    5
    Click Element    link=Create new account

    Input Text    name=firstname   john
    Input Text    name=lastname  wick
    Input Text    id=password_step_input   welcome123
    Select From List By Label    id=day    20
    Select From List By Index    id=month    11
    Select From List By Value    id=year    2020
    Click Element    xpath=//input[@value='-1']
    Click Element    xpath=//button[text()='Sign Up']
    

    