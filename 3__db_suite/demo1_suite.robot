*** Settings ***
Library     DatabaseLibrary

Suite Setup     Connect To Database    dbapiModuleName=pymysql  dbName=dbfree_db     dbPassword=12345678
    ...  dbHost=db4free.net     dbPort=3306   dbUsername=dbfree_db

Suite Teardown      Disconnect From Database
*** Test Cases ***
TC1

    ${row_count}    Row Count    select * from Products
    Log To Console    ${row_count}

TC2
    Row Count Is Equal To X    select * from Products    153
    Row Count Is Less Than X    select * from Products    201
    Row Count Is Greater Than X    select * from Products    10
    Row Count Is 0    select * from Products where product_id=

TC3
    ${desc}     Description    select * from Products
    Log To Console    ${desc}
TC4
    @{output}   Query    select * from Products
    Log    ${output}
    Log Many    ${output}
    Log Many    ${output}[0]
    Log Many    ${output}[0][1]

    ${size}     Get Length    ${output}
    Log    ${size}

    FOR    ${counter}    IN RANGE    1    ${size}
        Log    ${output}[${counter}][0]

    END

TC5
    DatabaseLibrary.Execute Sql String    insert into Products values('998899889','ashish','ashish dec')

TC7
    Execute Sql String    Insert into Products (product_id, ProductName, description) values (98987766,'ashishnew,'ashishdesc')
    Row Count Is Equal To X    select * from products where product_id='98987766'    1

