*** Settings ***
Library     RequestsLibrary

Test Template     Find valid Pet By Id
*** Test Cases ***

TC1
    5

TC2
    10

TC3
    3
*** Keywords ***
Find valid Pet By Id
    [Arguments]     ${pet}
    Create Session    alias=petshop    url=https://petstore.swagger.io/v2
    ${response}     GET On Session    alias=petshop    url=pet/${pet}    expected_status=200
    Log    ${response}
    Log    ${response.json()}
    Log    ${response.status_code}
    Status Should Be    200
    Log    ${response.json()}[id]
    Should Be Equal As Numbers    ${response.json()}[id]    5