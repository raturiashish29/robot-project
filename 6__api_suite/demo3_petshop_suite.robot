*** Settings ***
Library     RequestsLibrary
Library     OperatingSystem

Test Setup       Create Session    alias=petshop    url=https://petstore.swagger.io/v2
Test Teardown   Delete All Sessions

*** Test Cases ***
TC1 Add Valid Pet

    &{header_dic}   Create Dictionary   Content-type=application/json       Connection=keep-alive
    ${json}     Get Binary File    ${EXECDIR}${/}test_data${/}new_pet.json
    ${response}     POST On Session     alias=petshop   url=pet   headers=${header_dic}     data=${json}    expected_status=200
    Should Be Equal As Numbers    ${response.json()}[id]    6546
    Should Be Equal As Strings    ${response.json()}[name]   testdoggie

TC2 Update Valid Pet

    &{header_dic}   Create Dictionary   Content-type=application/json       Connection=keep-alive
    ${json}     Get Binary File    ${EXECDIR}${/}test_data${/}new_pet.json
    ${response}     PUT On Session   alias=petshop   url=pet   headers=${header_dic}     data=${json}    expected_status=200
    Should Be Equal As Numbers    ${response.json()}[id]    6546
    Should Be Equal As Strings    ${response.json()}[name]   testdoggie

TC2 Delete Valid Pet By Id
    Create Session    alias=petshop    url=https://petstore.swagger.io/v2
     &{header_dic}   Create Dictionary   api_key=2345678shish
    ${response}     DELETE On Session   alias=petshop  headers=${header_dic}  url=pet/6546   expected_status=200

TC3 Delete Invalid Pet By Id
    Create Session    alias=petshop    url=https://petstore.swagger.io/v2
     &{header_dic}   Create Dictionary   api_key=2345678shish
    ${response}     DELETE On Session   alias=petshop  headers=${header_dic}  url=pet/6546   expected_status=404


