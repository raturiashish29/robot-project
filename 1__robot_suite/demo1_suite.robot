*** Settings ***
Library    DateTime

*** Test Cases ***
TC1
  Log To Console  "Ashish"

TC2
  Log To Console  message="Hi Again"
  Log To Console  Ashu

TC3
  ${my_name}  Set Variable    ashish
  Log To Console    ${my_name}
  Log   ${my_name}
  
TC4
  ${radius}  Set Variable   10
  ${area}   Evaluate   3.14*${radius}*${radius}
  Log To Console    ${area}

  

TC5
  ${currnt_date}    Get Current Date
  Log To Console    ${currnt_date}
  Log    ${currnt_date}