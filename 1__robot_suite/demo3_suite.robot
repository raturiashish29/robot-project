*** Settings ***
Library    Collections
*** Variables ***
${BROWSER_NAME}     chrome
@{COLOURS}    red   yellow  blue
&{MY_DETAIL}    name=ashish     age=39      company=ericsson
*** Test Cases ***

TC4
    Log To Console    ${MY_DETAIL}[name]
    Log To Console    ${MY_DETAIL}[age]
TC1
    Log To Console    ${BROWSER_NAME}

TC2
    Log To Console    ${BROWSER_NAME}
    Log To Console    ${COLOURS}
    Log To Console    ${COLOURS}[1]
    ${size}     Get Length    ${COLOURS}
    Log To Console    ${size}

TC3
   @{fruits}    Create List     Mango    banana    apple
   ${size}     Get Length    ${fruits}
   Log To Console    ${size}
   Append To List    ${fruits}  grapes
   Log To Console    ${fruits}
   Remove From List    ${fruits}    2
   Log To Console    ${fruits}
   Insert Into List    ${fruits}    0    jackfruit
   Log To Console    ${fruits}

TC5
    &{opt}  Create Dictionary    platform=ios    app=zomato
    &{dic}  Create Dictionary    header=pplication/json     body=pet    opt=${opt}
    Log To Console    ${dic}
    Log    ${dic}