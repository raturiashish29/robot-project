*** Settings ***
Library     OperatingSystem

*** Test Cases ***

TC1
    Create Directory   path=C:${/}Automation Concepts${/}demodir
    Directory Should Exist    path=C:${/}Automation Concepts${/}demodir
    Remove Directory    path=C:${/}Automation Concepts${/}demodir
    Directory Should Not Exist    path=C:${/}Automation Concepts${/}demodir
    
TC2
    @{files}  List Files In Directory    path=C:${/}Program Files${/}WinRAR
    Log To Console    ${files}


TC3
    Log To Console    ${CURDIR}
    Log To Console    ${EXECDIR}
    Log To Console    ${OUTPUT_DIR}
    Log To Console    ${SUITE_NAME}
    Log To Console    ${TEST_NAME}
