*** Settings ***
Library     String
*** Test Cases ***
TC1
  ${name}    Set Variable   robot session
  ${name}    Convert To Upper Case    ${name}
  Log To Console    ${name}
  
TC2
  ${num1}   Set Variable   $102,000
  ${num2}   Set Variable   $201,500,000
  ${num1}   Remove String    ${num1}    $   ,
  ${num2}   Remove String    ${num2}    $   ,

  ${sum}    Evaluate    ${num1} + ${num2}
  ${num1}   Convert To Integer    ${num1}
  ${num2}   Convert To Integer    ${num2}
  Log   ${num1}
  Log   ${num2}

  Log To Console    ${sum}
  