*** Settings ***
Library     SeleniumLibrary
Library     AutoItLibrary
Library    FakerLibrary

Test Teardown   Run Keywords    Sleep    5s     AND     Close Browser

*** Test Cases ***
TC1 Windows Credentials Using URL
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://admin:admin@the-internet.herokuapp.com/basic_auth


TC2 Windows Credentials Using Autoit
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://the-internet.herokuapp.com/basic_auth
    Sleep    2s
    AutoItLibrary.Send      admin
    AutoItLibrary.Send      {TAB}
    AutoItLibrary.Send      admin
    AutoItLibrary.Send      {ENTER}


TC3 Windows Credentials Using Autoit
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://www.ilovepdf.com/pdf_to_word
    Click Element    id=pickfiles
    Sleep    5s
    Control Focus   Open    ${EMPTY}    Edit1
    Control Set Text    Open    ${EMPTY}    Edit1   C:${/}Automation Concepts${/}demofile.pdf
    Control Click       Open    ${EMPTY}    Button1


